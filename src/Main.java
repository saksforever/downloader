import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.nio.file.FileSystems;
import java.util.concurrent.*;

class Main {

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();

        String dir = "downloads";
        String currentDir = Paths.get("").toAbsolutePath().toString() + FileSystems.getDefault().getSeparator();
        String downloadsDir = currentDir + dir + FileSystems.getDefault().getSeparator();

        String site = "https://www.spbstu.ru/";
//        String site = "https://www.kinopoisk.ru/";

        List<URL> imageLinks = ImageLinks.extractLinksWithImages(site);

        List<URL> links = ExtractLinks.extractLinks(site);;


        ExecutorService pool1 = Executors.newFixedThreadPool(10);
        for (URL link : links) try {
            Future<List<URL>> result = pool1.submit(new ImageLinks(link.toString()));
            imageLinks.addAll(result.get());
        } catch (Exception ex) {
            System.out.println("error in link: " + link);
        }
        pool1.shutdown();
//        try {
//            if (!pool1.awaitTermination(20, TimeUnit.SECONDS)) {
//                pool1.shutdownNow();
//            }
//        } catch (InterruptedException e) {
//            pool1.shutdownNow();
//        }

        System.out.println("debug" + imageLinks.size());

        ExecutorService pool2 = Executors.newFixedThreadPool(50);
//        ExecutorService pool2 = Executors.newSingleThreadExecutor();
        for (URL link : imageLinks) {
            System.out.println("link " + link);
            pool2.submit(new DownloadTask(link, downloadsDir));
        }
        pool2.shutdown();
//        try {
//            if (!pool2.awaitTermination(10, TimeUnit.MINUTES)) {
////                20, TimeUnit.SECONDS
//                pool2.shutdownNow();
//            }
//        } catch (InterruptedException e) {
//            pool2.shutdownNow();
//        }

//        for (URL link : imageLinks) {
//            System.out.println("link " + link);
//            DownloadTask.downloadFile(link, currentDir);
//        }

        System.out.println("debug2");

        long endTime   = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println("Total time nanoSec: " + totalTime);
    }
}