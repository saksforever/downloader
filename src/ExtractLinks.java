import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class ExtractLinks implements Callable <List<URL>> {

    private String url;

    public ExtractLinks(String url) {
        this.url = url;
    }

    public static List<URL> extractLinks(String url) throws IOException {
        final ArrayList<URL> result = new ArrayList<>();

        try {
//            //System.out.println(link);
            Document doc = Jsoup.connect(url).ignoreHttpErrors(true).get();

//
//            //.ignoreContentType(true).ignoreHttpErrors(true)
//
            Elements links = doc.select("a[href^=http]");
//
//            // href ...
            for (Element link : links) {
                //result.addAll(extractLinksWithImages(link.attr("abs:href"), depth - 1));
                result.add(new URL(link.attr("abs:href")));
            }
        }  catch (ConnectException ce) {
            System.out.println(url + " Connection Exception");
        } catch (SocketException se) {
            System.out.println(url + " Socket Exception");
        } catch (Exception eee) {
            System.out.println(url + "  some exception");
            System.out.println(eee.getMessage());
        }

        return result;
    }

    public List<URL> call() {
        // surround with try-catch if downloadFile() throws something
        try {
            return extractLinks(url);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
