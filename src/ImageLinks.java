
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ImageLinks implements Callable<List<URL>> {

    private String url;

    public ImageLinks(String url) {
        this.url = url;

    }

    public static List<URL> extractLinksWithImages(String url) throws IOException {
        final ArrayList<URL> result = new ArrayList<URL>();
        try {
            Document doc = Jsoup.connect(url).ignoreContentType(true).ignoreHttpErrors(true).get();
            //.ignoreContentType(true).ignoreHttpErrors(true)

            // Elements links = doc.select("a[href^=http]");
            Elements media = doc.select("img");
            for (Element src : media) {
                try {
//            if (src.attr("abs:alt") = 'img'):
                    result.add(new URL(src.attr("abs:src")));
//            return result;
                } catch (MalformedURLException e) {
//            if (src.attr("abs:alt") = 'img'):
                    try {
                        result.add(new URL(src.attr("abs:data-src")));
                    } catch (MalformedURLException ee) {
                        System.out.println(src + " malformed");
                    }
                }
            }
        } catch (SocketTimeoutException e) {
            System.out.println(url + " SocketTimeoutException");
        }

        return result;
    }

    public List<URL> call() {
        // surround with try-catch if downloadFile() throws something
        try {
            return extractLinksWithImages(url);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}