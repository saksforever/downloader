import org.apache.commons.io.FilenameUtils;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

public class DownloadTask implements Runnable{

    private URL url;
    private final String Path;

    public DownloadTask(URL url, String Path) {
        this.url = url;
        this.Path = Path;
    }

    public static void downloadFile(URL url, String Path) throws Exception {
        String name = FilenameUtils.getName(url.getPath());

        int timeout = 3000;

        try {
            URLConnection conn = url.openConnection();
            conn.setConnectTimeout(timeout);
            if (conn.getContentLength() > 67000) {
                InputStream in = conn.getInputStream();
                System.out.println(Path + name);
                Files.copy(in, Paths.get(Path + name));
                System.out.println(Path + name + " write end");
                in.close();
            }
        } catch (FileAlreadyExistsException e) {
            System.out.println("file " + name  + " exists");
            Random random = new Random();
            String NewName = random.ints(97, 123)
                    .limit(5)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();
            URLConnection conn = url.openConnection();
            InputStream in = conn.getInputStream();
            System.out.println(Path + NewName + name);
            Files.copy(in, Paths.get(Path + NewName+ name));
            System.out.println(Path + NewName + name + " write end");
            in.close();
        }
    }


    public void run() {
        // surround with try-catch if downloadFile() throws something
        try {
            downloadFile(url, Path);
        } catch (Exception e) {
            //throw new RuntimeException(e);
        }
    }
}
